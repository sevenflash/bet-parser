var Q = require('q');
var util = require("util");
var cheerio = require("cheerio");
var rp = require('request-promise').defaults({
    transform: function (body) {
        return cheerio.load(body);
    }
});
var BetParser = require('./bet-parser.js');

function Surebet() {
    this.nextPageSelector = '.next_page';
    this.recordSelector = '.surebet_record';
    this.lineSelector = 'tr';
    this.lineGlobalSelector = '.surebet_record tr';
    this.itemsSelector = '.event';
    this.eventNameSelector = 'a';
    this.eventTypeSelector = 'span';
    this.sportTypeSelector = '.booker .minor';
    this.morePageSelector = '.extra a';

    this.baseURL = 'surebet.com';
    this.prefixURL = 'https://';

    this.databaseItems = null;
    this.parserID = 'surebet-teams';

    this.formatURL = function (url) {
        if (url.indexOf(this.prefixURL) !== -1) {
            this.prefixURL = url.substr(0, 11);
        }
        return this.constructor.super_.prototype.formatURL.call(this, url);
    };

    this.parse = function ($) {
        var self = this;
        var itemsPage = [];
        var promises = [];
        var nextPage = false;
        if ($(self.nextPageSelector).length > 0) {
            nextPage = $(self.nextPageSelector).attr('href');
        }
        if ($(self.recordSelector).length > 0) {
            $(self.recordSelector).each(function () {
                    var firstID = self.guid();
                    var secondID = self.guid();
                    if ($(this).find(self.lineSelector).length > 0) {
                        var morePage = $(this).find(self.morePageSelector).attr('href');
                        var $$ = $(this);
                        var recordPromise = Q.resolve($$);
                        var lineSelector = self.lineSelector;
                        if (morePage) {
                            recordPromise = rp(self.formatURL(morePage)).then(function ($) {
                                if ($(self.lineGlobalSelector).length > 0) {
                                    lineSelector = self.lineGlobalSelector;
                                    return $;
                                } else return $$;
                            });
                        }
                        recordPromise.then(function ($) {
                                var recordItemsCounter = 0;
                                $(lineSelector).each(function () {
                                    var event = $(this).find(self.itemsSelector).find(self.eventNameSelector).text();
                                    var type = $(this).find(self.sportTypeSelector).text();
                                    var teams = event.split('—');
                                    if (teams.length == 2) {
                                        var firstTeam = teams[0].trim();
                                        var secondTeam = teams[1].trim();
                                        itemsPage.push({
                                            id: firstID,
                                            name: firstTeam,
                                            type: type
                                        });
                                        itemsPage.push({
                                            id: secondID,
                                            name: secondTeam,
                                            type: type
                                        });
                                        recordItemsCounter += 2;
                                    }
                                });

                                for (let i = itemsPage.length - recordItemsCounter; i < itemsPage.length; i += 2) {
                                    if (self.databaseItems !== null) {
                                        for (let j = 0; j < self.databaseItems.length; j++) {
                                            if (itemsPage[i].name == self.databaseItems[j].name) {
                                                for (let k = itemsPage.length - recordItemsCounter; k < itemsPage.length; k += 2) {
                                                    itemsPage[k].id = self.databaseItems[j].id;
                                                }
                                                break;
                                            }
                                        }
                                    }
                                    for (let j = 0; j < itemsPage.length; j++) {
                                        if (itemsPage[i].name == itemsPage[j].name && itemsPage[i].id !== itemsPage[j].id) {
                                            for (let k = itemsPage.length - recordItemsCounter; k < itemsPage.length; k += 2) {
                                                itemsPage[k].id = itemsPage[j].id;
                                            }
                                            break;
                                        }
                                    }
                                }
                                for (let i = itemsPage.length - recordItemsCounter + 1; i < itemsPage.length; i += 2) {
                                    if (self.databaseItems !== null) {
                                        for (let j = 0; j < self.databaseItems.length; j++) {
                                            if (itemsPage[i].name == self.databaseItems[j].name) {
                                                for (let k = itemsPage.length - recordItemsCounter + 1; k < itemsPage.length; k += 2) {
                                                    itemsPage[k].id = self.databaseItems[j].id;
                                                }
                                                break;
                                            }
                                        }
                                    }
                                    for (let j = 1; j < itemsPage.length; j++) {
                                        if (itemsPage[i].name == itemsPage[j].name && itemsPage[i].id !== itemsPage[j].id) {
                                            for (let k = itemsPage.length - recordItemsCounter; k < itemsPage.length; k += 2) {
                                                itemsPage[k].id = itemsPage[j].id;
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                        );
                        promises.push(recordPromise);
                    }
                }
            );
        }
        return Q.allSettled(promises).then(function () {
            return {
                items: itemsPage,
                nextPage: nextPage
            };
        });
    }
    ;
}

util.inherits(Surebet, BetParser);

module.exports = function () {
    return new Surebet();
}();