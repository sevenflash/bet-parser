// SUREBET TEAMS PARSE EXAMPLE
var parser = require('./parser.js');
var db = require('./db.js');

var link = 'https://en.surebet.com/surebets'; //process.argv[2];

db.init(true, true, 'teams').then(function (items) {
    return parser.init(items, link, 'surebet-teams');
}).then(parser.parseItems)
    .then(function (result) {
        return db.filterItems(result, true);
    })
    .then(function(result) {
        return db.truncateTable().then(function() {
            return result;
        });
    })
    .then(function(result) {
        return db.saveItems(result, true);
    })
    .then(function () {
        console.log(link + ' was successfully parsed\n');
        return db.closeConnection();
    }).fail(console.error);