var parser = require('./parser.js');
var db = require('./db.js');
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var Q = require('q');

app.use(bodyParser());
app.post('/', function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    console.log('new request!');
    var initDB = db.init(req.body.save);
    var initParser = parser.init(req.body.url);
    Q.all([initDB, initParser])
        .then(function (result) {
            return parser.parseItems(result[1]);
        })
        .then(db.filterItems)
        .then(db.saveItems)
        .then(function () {
            res.send(req.body.url + ' was successfully parsed\n');
        });
});

app.listen(3000, function () {
    console.log('Parser listening on port 3000!');
});