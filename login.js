var fs = require('fs');
var Q = require('q');
var cheerio = require('cheerio');
var rp = require('request-promise');

function saveCookies(cookies, fileName) {
    cookies = cookies.map(function (cookie) {
        return cookie.split(';')[0];
    });
    cookies = cookies.join(';');
    return Q.nfcall(fs.writeFile, fileName, cookies).then(function () {
        return cookies;
    });
}

function login(settings) {
    var authOptions = {
        method: 'POST',
        uri: settings.authURL,
        form: settings.authForm,
        resolveWithFullResponse: true,
        followRedirect: false
    };
    return Q.nfcall(fs.readFile, settings.cookieFileName, "utf-8").then(function (cookies) {
        var testAuthOptions = {
            uri: settings.testAuthURL,
            headers: {
                'Cookie': cookies
            },
            transform: function (body) {
                return cheerio.load(body);
            },
            followRedirect: false
        };
        return rp(testAuthOptions).then(function ($) {
            if ($(settings.loginCheckSelector).length) {
                return rp(authOptions).then(function (response) {
                    return saveCookies(response.headers['set-cookie'], settings.cookieFileName);
                }).catch(function (reason) {
                    var response = reason.response;
                    if (response.statusCode !== 302 && response.statusCode !== 200) {
                        return false;
                    }
                    return saveCookies(response.headers['set-cookie'], settings.cookieFileName);
                });
            } else {
                return cookies;
            }
        }).catch(function (reason) {
            var response = reason.response;
            if (response.statusCode !== 302 && response.statusCode !== 200) {
                return false;
            }
            return saveCookies(response.headers['set-cookie'], settings.cookieFileName);
        });
    }).fail(function () {
        return rp(authOptions).then(function (response) {
            return saveCookies(response.headers['set-cookie'], settings.cookieFileName);
        }).catch(function (reason) {
            var response = reason.response;
            if (response.statusCode !== 302 && response.statusCode !== 200) {
                return false;
            }
            return saveCookies(response.headers['set-cookie'], settings.cookieFileName);
        });
    });
}

module.exports = login;