-- MySQL dump 10.13  Distrib 5.7.12, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: bet
-- ------------------------------------------------------
-- Server version	5.6.32-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `coeffs`
--

DROP TABLE IF EXISTS `coeffs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coeffs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coef1` varchar(255) DEFAULT NULL,
  `coef2` varchar(255) DEFAULT NULL,
  `coef3` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coeffs`
--

LOCK TABLES `coeffs` WRITE;
/*!40000 ALTER TABLE `coeffs` DISABLE KEYS */;
INSERT INTO `coeffs` VALUES (1,'Ф2(+0.25) (1-й период)','Ф1(0.0) (1-й период)','1 (1-й период)'),(2,'Ф2(+1.0)','1','Ф1(−1.5)'),(3,'Тм(1) для 2-й команды','Тб(0.5) для 2-й команды','Тб(1.5) для 2-й команды (после 90 минут)'),(4,'x_ret1','1','2'),(5,'Ф1(0.0)','X','2'),(6,'Ф1(−3.0)','Ф2(+3.5)','Ф2(+2.5)'),(7,'Тм(4)','Тб(3.5)','Тб(4.5)'),(8,'Ф2(+0.25)','X','1'),(9,'Ф2(0.0)','X','1'),(10,'П1 / DNB','X','2'),(11,'П1 / DNB','X','2'),(12,'П1 / DNB','X','2'),(13,'Ф1(−1.0)','Ф2(+1.5) (после 60 минут)','X2'),(14,'Ф2(−1.0)','ГX(1:0)','Г1(1:0)'),(15,'Ф1(+1.0)','2','Г2(1:0)'),(16,'2 и Тб(2.5)','2 и Тм(2.5)','2 - против'),(17,'Ф1(−1.75)','Ф2(+2.0)','Ф2(+1.5)'),(18,'Тотал 6 or more','Тм(5.5)',NULL),(19,'Ф1(−1.75)','ГX(0:2)','Г2(0:2)'),(20,'1','X2',NULL),(21,'Ф1(0.0)','X','2'),(22,'Тб(155) (с ОТ)','Тм(155.5) (с ОТ)','Тм(154.5) (с ОТ)'),(23,'Тб(1)','ровно 1','0:0'),(24,'Ф1(0.0)','X','2'),(25,'Ф1(0.0)','Ф2(0.0)',NULL),(26,'Тб(0.5) для 1-й команды','Тм(0.5) для 1-й команды',NULL),(27,'Ф1(0.0)','Ф2(0.0)',NULL),(28,'Тб(1)','Тм(1.5)','0:0'),(29,'Ф1(0.0)','X','2'),(30,'Тм(1) для 2-й команды','Тб(0.5) для 2-й команды','Тб(1.5) для 2-й команды'),(31,'Тб(185) (с ОТ)','Тм(185.5) (с ОТ)','Тм(184.5)'),(32,'Тм(1) для 2-й команды','Тб(0.5) для 2-й команды','Тб(1.5) для 2-й команды'),(33,'Ф1(0.0)','Ф2(0.0)',NULL),(34,'Тм(5)','Тб(4.5)','Тб(5.5) (после 90 минут)'),(35,'Ф2(+0.75)','1','Ф1(−1.5)'),(36,'Ф2(+2.0)','Ф1(−1.5)','Ф1(−2.5)'),(37,'Ф1(+1.75)','Ф2(−1.5)','Г2(2:0) (после 90 минут)'),(38,'1','X','2'),(39,'П2 / DNB','Ф1(+0.5)','1'),(40,'Тб(18.5) - геймы','Тм(18.5) - геймы',NULL),(41,'Ф1(0.0)','П2 / DNB',NULL),(42,'Тб(1) для 1-й команды','Тм(1) для 1-й команды',NULL),(43,'Тб(1) (1-й период)','Тм(1) (1-й период)',NULL),(44,'Тб(1) для 2-й команды','Тм(1) для 2-й команды',NULL),(45,'Ф1(−6.5) (с ОТ)','Ф2(+6.5) (с ОТ)',NULL),(46,'Тб(6.5) - угловые для 1-й команды','Тм(6.5) - угловые для 1-й команды',NULL),(47,'П1 / DNB','Ф2(0.0)',NULL),(48,'Ф2(+1.0)','1','Г1(0:1)'),(49,'Ф2(+1.0)','1','Г1(0:1)'),(50,'1','X','Ф1(−0.5)'),(51,'Ф2(+0.25)','X','1'),(52,'1','X','2'),(53,'Тм(4)','Тб(3.5)','Тб(4.5)'),(54,'X - против','X',NULL),(55,'1 - против','1',NULL),(56,'Ф1(+0.75)','1','Г1(0:1)'),(57,'Ф2(+0.25)','X','1'),(58,'Тб(10.5) - геймы (1-й сет)','Тм(10.5) - геймы (1-й сет)',NULL),(59,'Ф1(0.0)','Ф2(0.0)',NULL),(60,'Тб(1.5) для 1-й команды (1-й период)','Тм(1.5) для 1-й команды (1-й период)',NULL),(61,'равное число голов в обеих половинах - нет','равное число голов в обеих половинах',NULL),(62,'x_ret2','2','1'),(63,'Ф1(0.0)','X','2'),(64,'Ф1(−0.5) (после 90 минут)','X','2'),(65,'Ф2(0.0)','Г1(1:0)','Ф1(−0.25)'),(66,'П1 / DNB','X','2'),(67,'Ф1(0.0)','X','2'),(68,'Тм(1) (1-й период)','Тб(0.5) (1-й период)','Тб(1.5) (1-й период)'),(69,'Тб(1)','Тм(1.5)','0:0'),(70,'Тб(2.75)','ровно 3','Тм(2.5)'),(71,'Тм(5)','ровно 5','Тотал 6 or more'),(72,'1','X (после 90 минут)','2'),(73,'Тб(4)','Тм(4.5)','Тм(3.5)'),(74,'Ф1(0.0)','X','2'),(75,'Ф2(0.0)','Ф1(+0.5)','1'),(76,'Ф2(0.0)','1X','1'),(77,'Тб(0.75) для 2-й команды','Тм(1) для 2-й команды','Тм(0.5) для 2-й команды'),(78,'Ф2(−1.0)','Ф1(+1.5)','Г1(1:0)'),(79,'Ф2(+4.75)','Ф1(−4.5)','Г1(0:5)'),(80,'Тм(6)','Тб(5.5)','Тб(6.5)'),(81,'Ф2(+0.25)','X','1'),(82,'П2 / DNB','X','1'),(83,'Тм(159) (с ОТ)','Тб(158.5) (с ОТ)','Тб(159.5) (с ОТ)'),(84,'1','X','2'),(85,'Ф1(0.0)','X','2'),(86,'Ф2(+1.75)','Ф1(−1.5)','Ф1(−2.5)'),(87,'X2 и Тб(5.5)','X2 и Тм(5.5)','1'),(88,'Тм(3)','Тб(2.5)','Тб(3.5)'),(89,'Тм(1) (1-й период)','Тб(0.75) (1-й период)','Тб(1.5) (1-й период)'),(90,'П1 / DNB','Ф2(+0.5)','2'),(91,'Ф1(−2.0)','Ф2(+2.0)',NULL),(92,'Тотал 5 or more','Тм(4.5)',NULL),(93,'Тб(1) для 1-й команды','Тм(1) для 1-й команды',NULL),(94,'1','X2',NULL),(95,'Тотал 5 or more','Тм(4.5)',NULL),(96,'П1 / DNB','X','2'),(97,'Ф1(−1.0)','Ф2(+1.5)','X2'),(98,'Тб(4.5)','Тотал 3-4','Тм(2.5)'),(99,'Ф1(+1.0)','2','Ф2(−1.5)'),(100,'Тб(3.5)','ровно 3','Тм(2.5)'),(101,'Тм(5)','Тб(4.5)','Тм(5.5) - против'),(102,'Тм(4)','ровно 4','Тб(4.5)'),(103,'Ф2(−0.25)','X (после 90 минут)','1'),(104,'1','X','2'),(105,'П2 / DNB','X','1'),(106,'Тм(196) (с ОТ)','Тб(195.5) (с ОТ)','Тб(196.5) (с ОТ)'),(107,'Ф2(+1.0)','1','Ф1(−1.5)'),(108,'Ф1(0.0)','X','2'),(109,'П1 / DNB (1-й период)','Ф2(+0.25) (1-й период)','2 (1-й период)'),(110,'Ф2(+2.0) (1-й период)','Ф1(−1.5) (1-й период)','Г1(0:2) (1-й период)'),(111,'x_ret1','1','2'),(112,'Ф2(0.0)','X','1'),(113,'Ф2(0.0)','Ф1(+0.5)','1'),(114,'Ф1(−6.0) (с ОТ)','Ф2(+6.5) (с ОТ)','Ф2(+5.5) (с ОТ)'),(115,'Ф1(0.0)','Ф2(+0.5)','2'),(116,'Ф1(0.0)','X','2'),(117,'Ф2(0.0) (1-й период)','X (1-й период)','1 (1-й период)'),(118,'Тб(2) для 1-й команды','Тм(2) для 1-й команды',NULL),(119,'1 (после 60 минут)','Ф2(+0.5)',NULL),(120,'Ф1(−2.0)','Ф2(+2.0)',NULL),(121,'Тб(1.5) для 2-й команды','Тм(1.5) для 2-й команды',NULL),(122,'Ф1(0.0)','X2','2'),(123,'П1 / DNB','X','2'),(124,'П1 / DNB','X','2 (после 90 минут)'),(125,'Ф2(+2.0)','ГX(0:2)','Ф1(−2.5)'),(126,'Ф2(+2.0)','ГX(0:2)','Ф1(−2.5)'),(127,'П2 / DNB','X','1'),(128,'Ф2(+7.0) (с ОТ)','Ф1(−6.5) (с ОТ)','Ф1(−7.5) (с ОТ)'),(129,'Ф2(0.0)','X','1'),(130,'Ф2(+2.0)','Г1(0:1)','Ф1(−2.5)'),(131,'1 (всухую)','Тб(0.5) для 2-й команды','0:0'),(132,'Ф2(0.0)','X','1'),(133,'Ф2(+2.0)','Ф1(−1.75)','Ф1(−2.5)'),(134,'П1 / DNB','1 - против','2'),(135,'Ф2(+4.0)','Ф1(−3.5)','Ф1(−4.5)'),(136,'Ф1(+1.0)','2','Г2(1:0) (после 90 минут)'),(137,'Ф1(−2.0)','Ф2(+2.5)','Ф2(+1.5)'),(138,'Ф2(−1.0)','Ф1(+1.5)','1X'),(139,'Ф1(−1.0) (после 90 минут)','ГX(0:1) (после 90 минут)','X2'),(140,'Тм(5)','Тб(4.5)','Тб(5.5)'),(141,'Тм(3) (после 90 минут)','Тб(2.5)','Тб(3.5)'),(142,'Тб(80.5) для 1-й команды (с ОТ)','Тм(80.5) для 1-й команды (с ОТ)',NULL),(143,'Тм(1) для 2-й команды','Тб(0.75) для 2-й команды','Тб(1.5) для 2-й команды'),(144,'Ф1(0.0)','X','2'),(145,'Ф1(+1.0)','2','Ф2(−1.25)'),(146,'П1 / DNB','X','2'),(147,'Тб(3.5)','Тм(3.5)',NULL),(148,'Ф1(0.0)','X','2'),(149,'Ф1(0.0)','X','2'),(150,'Ф1(−1.75)','Ф2(+2.0)','Ф2(+1.5)'),(151,'Тотал 6 or more','Тм(5.5)',NULL),(152,'Ф1(+1.0)','2','Ф2(−1.5)'),(153,'Ф2(+0.75)','1','Г1(0:1)'),(154,'Тм(4)','Тотал 4 or more','Тотал 5 or more'),(155,'Тб(1.25) (1-й период)','Тм(1.5) (1-й период)','Тм(0.5) (1-й период)'),(156,'Ф2(0.0)','X','1'),(157,'Ф2(−1.0)','Г1(2:0)','1X'),(158,'Ф2(+3.0)','ГX(0:3)','Г1(4 or more)'),(159,'Тотал 4 or more для 2-й команды','Тотал 2-3 для 2-й команды','Тм(1.5) для 2-й команды'),(160,'Тм(1) для 2-й команды','Тб(0.75) для 2-й команды','Тб(1.5) для 2-й команды'),(161,'П1','П2',NULL),(162,'Тб(2)','Тм(2.5)','Тм(1.5)'),(163,'П1 / DNB','X','2'),(164,'1','X','2'),(165,'Тб(5.5)','ровно 5','Тм(4.5)'),(166,'Тб(1) (1-й период)','Тм(1.5) (1-й период)','Тм(0.5) (1-й период)'),(167,'1','X','2'),(168,'П2 / DNB - против','X2','2'),(169,'Ф2(+0.25)','X','1'),(170,'Тб(0.75) (1-й период)','Тм(1) (1-й период)','Тм(0.5) (1-й период)'),(171,'П1','П2',NULL),(172,'Тб(2) для 1-й команды','Тм(2) для 1-й команды',NULL),(173,'Тм(4)','Тб(3)','Тб(4.5)'),(174,'Тм(2.75)','Тб(2.5)','Тб(3.5)'),(175,'Тб(1)','Тм(1.5) (после 90 минут)','0:0'),(176,'П1 / DNB','X2','2'),(177,'Тб(83) для 1-й команды (с ОТ)','Тм(83.5) для 1-й команды (с ОТ)','Тм(82.5) для 1-й команды (с ОТ)'),(178,'Ф1(−1.0)','Ф2(+1.0)',NULL),(179,'Тм(1) для 1-й команды','Тб(0.5) для 1-й команды','Тб(1.5) для 1-й команды'),(180,'1','X','2'),(181,'ГX(0:3) - нет','ГX(0:3)',NULL),(182,'Тб(2.75)','Тм(3)','Тм(2.5)'),(183,'П1 (1-й сет)','П2 (1-й сет)',NULL),(184,'Тб(1) (1-й период)','Тм(1) (1-й период)',NULL),(185,'Тб(156) (с ОТ)','Тм(156) (с ОТ)',NULL),(186,'Тб(5.5)','Тм(5.5)',NULL),(187,'Тб(1.5) (1-й период)','Тм(1.5) (1-й период)',NULL),(188,'Тб(1) (1-й период)','Тм(1) (1-й период)',NULL),(189,'Тб(1) (1-й период)','Тм(1) (1-й период)',NULL),(190,'Ф1(0.0)','Ф2(0.0)',NULL),(191,'1 (1-й период)','X2 (1-й период)',NULL),(192,'Тб(4)','Тм(4.5)','Тм(3.5)'),(193,'1','X','2'),(194,'П2 / DNB','X','1'),(195,'1','X','2'),(196,'Тб(1) (2-й период)','Тм(1.5) (2-й период)','Тм(0.5) (2-й период)'),(197,'П2 / DNB','X','1'),(198,'Ф2(−0.25)','X','Ф1(0.0)'),(199,'Ф2(0.0)','X','1'),(200,'Ф1(+2.0)','Ф2(−1.5)','Ф2(−2.5)');
/*!40000 ALTER TABLE `coeffs` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-10-07 11:41:34
