function BetParser() {
}
BetParser.prototype = {
    baseURL: 'example.com',
    prefixURL: 'https://www.',

    nextPageSelector: '',

    cookies: null,
    authURL: false,
    testAuthURL: false,
    authForm: false,

    formatURL: function (url) {
        if (!url.startsWith(this.prefixURL)) {
            if (url.indexOf(this.baseURL) >= 0) {
                // http://example.cokm/foobar => https://www.example.com/foobar
                url = this.prefixURL + this.baseURL + '/' + url.substr(url.indexOf(this.baseURL) + this.baseURL.length);
            } else {
                // /foobar => https://www.example.com/foobar
                url = this.prefixURL + this.baseURL + url;
            }
        }
        return url;
    },
    guid: function () {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }

        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    },
    parse: function ($) {
        return false;
    }
};
module.exports = BetParser;