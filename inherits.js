var util = require('util');

function First() {

}
First.prototype = {
    methodFirst:  function () {
        console.log('methodFirst');
    }
};

function Second() {
    this.methodSecond = function () {
        console.log('methodSecond');
    };
}

util.inherits(Second, First);

var test = new Second();
test.methodSecond();
test.methodFirst();