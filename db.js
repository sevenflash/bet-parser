var Q = require('q');
var fs = require('fs');
var mysql = require('mysql');

var connection = null;
var needToSave = false;
var itemsDatabase = null;
var tableName = null;

function init(needToSaveFlag, needGetItemsFlag, setTableName) {
    needToSave = needToSaveFlag;
    tableName = setTableName || 'teams';
    return Q.Promise(function (resolve, reject) {
        if (connection == null || (itemsDatabase == null && needGetItemsFlag)) {
            return createConnection().then(function (connectionCreated) {
                connection = connectionCreated;
                if (needGetItemsFlag) {
                    getItems().then(function (items) {
                        itemsDatabase = items;
                        return resolve(items);
                    });
                }
                return resolve(true);
            });
        }
        return resolve(true);
    });
}

function createConnection() {
    return Q.Promise(function (resolve, reject) {
        fs.readFile('db.txt', 'utf8', function (err, data) {
            if (err) reject(err);
            var DB_params = JSON.parse(data);
            connection = mysql.createConnection(DB_params);
            connection.connect();
            resolve(connection);
        });
    });
}

function filterItems(items, remove) {
    return Q.fcall(function() {
        var flags = [], filteredItems = [], l = items.length, i;
        for(i=0; i<l; i++) {
            if( flags[items[i].name]) continue;
            flags[items[i].name] = true;
            filteredItems.push(items[i]);
        }
        items = filteredItems;
        return items;
    });
}

function getItems() {
    var deferred = Q.defer();
    connection.query('SELECT * FROM '+tableName, function (err, res) {
        if (err) deferred.reject();
        deferred.resolve(res);
    });
    return deferred.promise;
}


function saveItems(items) {
    var promises = [];
    items.forEach(function (item) {
        var promise = saveToDB(item);
        promises.push(promise);
    });
    return Q.allSettled(promises);
}

function truncateTable() {
    var deferred = Q.defer();
    connection.query('TRUNCATE TABLE ' + tableName, function(err, res) {
       return deferred.resolve();
    });
    return deferred.promise;
}

function saveToDB(item) {
    console.log('Saving: ', item);
    var deferred = Q.defer();
    connection.query('INSERT INTO ' + tableName + ' SET ?', item, function (err, res) {
        if (!err) return deferred.resolve();
        deferred.reject();
    });
    return deferred.promise;
}

function closeConnection() {
    connection.end();
}

exports.init = init;
exports.saveItems = saveItems;
exports.filterItems = filterItems;
exports.closeConnection = closeConnection;
exports.truncateTable = truncateTable;