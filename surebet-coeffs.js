var Q = require('q');
var util = require("util");
var BetParser = require('./bet-parser.js');

function SurebetCoeffs() {
    this.nextPageSelector = '.next_page';
    this.recordSelector = '.surebet_record';
    this.lineSelector = 'tr';
    this.itemsSelector = '.event';
    this.eventNameSelector = 'a';
    this.eventTypeSelector = 'span';
    this.coeffSelector = '.coeff abbr';

    this.baseURL = 'surebet.com';
    this.prefixURL = 'https://';

    this.databaseItems = null;
    this.parserID = 'surebet-coeffs';

    this.formatURL = function(url) {
        if(url.indexOf(this.prefixURL) !== -1) {
            this.prefixURL = url.substr(0, 11);
        }
        return this.constructor.super_.prototype.formatURL.call(this, url);
    };

    this.parse = function ($) {
        var self = this;
        var itemsPage = [];
        var nextPage = false;
        if ($(self.nextPageSelector).length > 0) {
            nextPage = $(self.nextPageSelector).attr('href');
        }
        if ($(self.recordSelector).length > 0) {
            $(self.recordSelector).each(function () {
                if ($(this).find(self.lineSelector).length > 0) {
                    var coeffCounter = 0;
                    var coeffs = {};
                    $(this).find(self.lineSelector).each(function () {
                        coeffCounter++;
                        var coeff = $(this).find(self.coeffSelector).text();
                        if (coeff.length > 0) {
                            var coeffName = 'coef'.concat(coeffCounter);
                            coeffs[coeffName] = coeff;
                        }
                    });
                    itemsPage.push(coeffs);
                }
            });
        }
        return Q.resolve({
            items: itemsPage,
            nextPage: nextPage
        });
    };
}

util.inherits(SurebetCoeffs, BetParser);

module.exports = function () {
    return new SurebetCoeffs();
}();