// SUREBET COEFFS PARSE EXAMPLE
var parser = require('./parser.js');
var db = require('./db.js');

var link = 'https://en.surebet.com/surebets'; //process.argv[2];

db.init(true, true, 'coeffs').then(function (items) {
    return parser.init(items, link, 'surebet-coeffs');
}).then(parser.parseItems)
    .then(db.saveItems)
    .then(function () {
        db.closeConnection();
        console.log(link + ' was successfully parsed\n');
    }).fail(console.error);