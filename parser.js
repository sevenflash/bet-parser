var fs = require('fs');
var Q = require('q');
var login = require('./login.js');
var cheerio = require('cheerio');
var rp = require('request-promise').defaults({
    transform: function (body) {
        return cheerio.load(body);
    }
});

var SurebetParser = require('./surebet.js');
var SurebetParserCoeffs = require('./surebet-coeffs.js');

var parsersList = [SurebetParser, SurebetParserCoeffs];
var items = [];

function Parser() {
    var siteParser = null;
    var requestURL = null;
    var databaseItems = null;

    function init(items, url, parserID) {
        if (parserID) {
            parsersList.forEach(function (currentParser) {
                if ('parserID' in currentParser && currentParser.parserID == parserID) {
                    if (items.constructor === Array && 'databaseItems' in currentParser) currentParser.databaseItems = items;
                    siteParser = currentParser;
                }
            });
        }
        if (siteParser === null) {
            parsersList.forEach(function (currentParser) {
                if (url.indexOf(currentParser.baseURL) >= 0) {
                    if (items.constructor === Array && 'databaseItems' in currentParser) currentParser.databaseItems = items;
                    siteParser = currentParser;
                }
            });
        }
        if (siteParser === null) {
            return Q.reject('Unresolved URL');
        }
        else {
            requestURL = url;
            return Q.resolve(url);
        }
    }

    function parseItems(page) {
        if (!page) {
            page = requestURL;
        }
        return Q.Promise.resolve(page).then(parsePage).then(function (result) {
            items = items.concat(result.items);
            if (result.nextPage) {
                return parseItems(result.nextPage);
            } else {
                return Q.resolve(items);
            }
        });
    }

    function parsePage(url) {
        return rp({
            uri: siteParser.formatURL(url)
        }).then(siteParser.parse.bind(siteParser));
    }

    this.init = init;
    this.parseItems = parseItems;
}

module.exports.init = function (items, url, parserID) {
    var parser = new Parser();
    return parser.init(items, url, parserID).then(function () {
        return parser;
    });
};

module.exports.parseItems = function (parser) {
    return parser.parseItems();
};